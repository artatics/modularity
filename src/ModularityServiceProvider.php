<?php

namespace artatics\modularity;

use Illuminate\Support\ServiceProvider;

class ModularityServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /* Include booting of configuration file */
        $this->mergeConfigFrom(__DIR__ . '/config/modules.php', 'modules');
        $this->publishes([
            __DIR__ . '/config' => base_path('config'),
        ], 'modularity-config');
        $this->publishes([
            __DIR__ . '/app' => base_path('app'),
            __DIR__ . '/public' => base_path('public'),
        ], 'modularity-welcome');
        $modules = config('modules.include');
        $dir = app_path('Modules');
        if ($modules) {
            foreach ($modules as $module) {
                /* Include booting of Migration */
                if (is_dir($dir . '/' . $module . '/Migrations')) {
                    $this->loadMigrationsFrom($dir . '/' . $module . '/Migrations');
                }
                /* Include booting of Route */
                if (file_exists($dir . '/' . $module . '/Routes/routes.php')) {
                    $this->loadRoutesFrom($dir . '/' . $module . '/Routes/routes.php');
                }
                /* Include booting of Translations */
                if (is_dir($dir . '/' . $module . '/Languages')) {
                    $this->loadTranslationsFrom($dir . '/' . $module . '/Languages', $module);
                }
                /* Include booting of View */
                if (is_dir($dir . '/' . $module . '/Views')) {
                    $this->loadViewsFrom($dir . '/' . $module . '/Views', $module);
                }
            }
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}