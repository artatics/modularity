<?php

namespace App\Modules\Welcome\Controllers;

use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('Welcome::welcome');
    }
}