<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@lang('Welcome::language.description')</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('modules/welcome/css/style.css') }}" rel="stylesheet">
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title">
            @lang('Welcome::language.name')
        </div>
        <div class="text m-b-md">@lang('Welcome::language.description')</div>
        <div class="links">
            <a href="http://artatics.ru" target="_blank">Artatics</a>
            <a href="https://bitbucket.org/artatics/modularity/src/master/readme.md" target="_blank">Documentation</a>
            <a href="https://bitbucket.org/artatics/modularity/src/master/" target="_blank">Bitbucket</a>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Scripts -->
<script src="{{ asset('modules/welcome/js/main.js') }}"></script>
</body>
</html>