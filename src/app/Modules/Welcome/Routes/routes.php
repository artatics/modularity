<?php

Route::group(['namespace' => 'App\Modules\Welcome\Controllers'], function () {
    Route::get('/welcome', ['uses' => 'WelcomeController@index']);
});