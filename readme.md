Modularity
===

Modular system for Laravel.

Requirements
---

 - Laravel 5.6+
 - PHP 7.1+

### Installation

After creating your new Laravel application you can include the Modularity package with the following command:
```
composer require artatics/modularity
```
Installation package completed.

### Initialization

After installing the package, you need to include `ServiceProvider` in configuration files `config/app.php`
```
'providers' => [
    ...
    artatics\modularity\ModularityServiceProvider::class,
    ...
],
```

Create a configuration file, run the console command:
```
php artisan vendor:publish --tag=modularity-config
```

Install the demo module, run the console command:
```
php artisan vendor:publish --tag=modularity-welcome
```

### Using
Include module `Welcome` in configuration files `config/units.php`
```
'include' => [
    'Welcome'
],
```

You can check the module at `/welcome`:
```
example.loc/welcome
```

This module is successfully include!

### License

This package is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)